#ifndef APPLICATION_H
#define APPLICATION_H

#include "glew/glew.h"
#include "glfw/glfw3.h"
#include <stdlib.h>

#include "Box2D/Box2D.h"
#include "DebugDraw.h"
#include "Biped.h"

class Application;

#define	RAND_LIMIT	32767
#define DRAW_STRING_NEW_LINE 16
const int ID_GROUND = 99;
const int ID_BALL = 55;

/// Random number in range [-1,1]
inline float32 RandomFloat() {
	float32 r = (float32)(rand() & (RAND_LIMIT));
	r /= RAND_LIMIT;
	r = 2.0f * r - 1.0f;
	return r;
}

/// Random floating point number in range [lo, hi]
inline float32 RandomFloat(float32 lo, float32 hi) {
	float32 r = (float32)(rand() & (RAND_LIMIT));
	r /= RAND_LIMIT;
	r = (hi - lo) * r + lo;
	return r;
}

/// Test settings. Some can be controlled in the GUI.
struct Settings {
	Settings() {
		launchBalls = false;
		showCOM = false;
		drawJoints = false;
		drawContactPoints = false;
		drawContactNormals = false;
		steps = false;
		optimization = false; // passer � vrai pour activer l'optimisation
		optiDuration = 5.0;
		interpol_type = 1;
		radius = 0.2f;
		length = 0.4f;
		points = 6;
		gravity = -9.81f;
		ground = b2Vec2(50.0f, 0.0f);
		acceleration = 0.0;
	}

	bool launchBalls;
	bool showCOM;
	bool drawJoints;
	bool drawContactPoints;
	bool drawContactNormals;
	bool optimization;
	float optiDuration;
	bool steps;
	unsigned int interpol_type;
	float radius, length, gravity, acceleration;
	int points;
	bool extensions;
	b2Vec2 ground;
};

// This is called when a joint in the world is implicitly destroyed
// because an attached body is destroyed. This gives us a chance to
// nullify the mouse joint.
class DestructionListener : public b2DestructionListener {
public:
	void SayGoodbye(b2Fixture* fixture) override { B2_NOT_USED(fixture); }
	void SayGoodbye(b2Joint* joint) override;
	Application* app;
};

const int32 k_maxContactPoints = 2048;

struct ContactPoint {
	b2Fixture* fixtureA;
	b2Fixture* fixtureB;
	b2Vec2 normal;
	b2Vec2 position;
	b2PointState state;
	float32 normalImpulse;
	float32 tangentImpulse;
	float32 separation;
};

class Application : public b2ContactListener {
public:
	Application();
	Application(float grav, b2Vec2 ground);
	virtual ~Application();

	void DrawTitle(const char *string);
	virtual void Step(Settings* settings);
	virtual void Keyboard(int key) { B2_NOT_USED(key); }
	virtual void KeyboardUp(int key) { B2_NOT_USED(key); }
	virtual void MouseDown(const b2Vec2& p);
	virtual void MouseUp(const b2Vec2& p);
	void MouseMove(const b2Vec2& p);

	float getRatioKpKd() const{return m_biped->KpKvRatio;}
	float getKpCheville() const{return m_biped->KpCheville;}
	float getKpGenou() const{return m_biped->KpGenou;}
	float getKpHanche() const{return m_biped->KpHanche;}
	float getKpTronc() const{return m_biped->KpTronc;}
	float getTmpsPoses() const{if(m_biped->genericInterpose != 0.0) return m_biped->genericInterpose;}
	float setTmpsPoses(const float & val) const{m_biped->genericInterpose = val;}


	float setRatioKpKd(const float & val) const{ m_biped->KpKvRatio = val;}
	float setKpCheville(const float & val) const{ m_biped->KpCheville = val;}
	float setKpGenou(const float & val) const{ m_biped->KpGenou = val;}
	float setKpHanche(const float & val) const{ m_biped->KpHanche = val;}
	float setKpTronc(const float & val) const{ m_biped->KpTronc = val;}

	void changeBipedInterpol(unsigned int interpol_type);
	float BipedPosition() const {return m_biped->getCOM().x;}
	bool BipedHasFallen() const {return m_biped->hasFallen();}
	void setOptimizationData(const float * data);
	void getOptimizationData(float * data);
	void LaunchBall(float radius);
	void LaunchPolygon(int numberPoints, float length);

	void updateBipedPDController() const{ m_biped->updatePDController();}
	void changeAnimStand(){m_biped->changeFSMStand();};
	void changeAnimWalk(){m_biped->changeFSMWalk();};
	void changeAnimRun(){m_biped->changeFSMRun();};

	void accelerate(const b2Vec2& force);

	float getCurrentCost() const {return m_biped->sumAngleVelocity();}

	// Let derived tests know that a joint was destroyed.
	virtual void JointDestroyed(b2Joint* joint) { B2_NOT_USED(joint); }
	// Callbacks for derived classes.
	virtual void BeginContact(b2Contact* contact)  override { B2_NOT_USED(contact); }
	virtual void EndContact(b2Contact* contact)  override { B2_NOT_USED(contact); }
	virtual void PreSolve(b2Contact* contact, const b2Manifold* oldManifold) override;
	virtual void PostSolve(b2Contact* contact, const b2ContactImpulse* impulse) override
	{
		B2_NOT_USED(contact);
		B2_NOT_USED(impulse);
	}

	void ShiftOrigin(const b2Vec2& newOrigin);

protected:
	friend class DestructionListener;
	friend class BoundaryListener;
	friend class ContactListener;

	b2AABB m_worldAABB;
	ContactPoint m_points[k_maxContactPoints];
	int32 m_pointCount;
	DestructionListener m_destructionListener;
	int32 m_textLine;
	b2World* m_world;
	b2MouseJoint* m_mouseJoint;
	b2Vec2 m_mouseWorld;
	int32 m_stepCount;
	b2Profile m_maxProfile;
	b2Profile m_totalProfile;

	b2Body* m_groundBody;                   // le sol
	b2Body* m_ball;                         // la balle
	b2Body*	m_COM;		                    // le CdM
	Biped * m_biped;                        // le bip�de � animer
	double m_lastShoot;                     // temps du dernier lancement de balle
	double m_lastUpdate;                    // temps de la derni�re mise � jour
	double m_walkDistance;                  // distance parcourue (en metres)
	std::vector<b2Body*> m_steps;           // les marches sur le sol
	std::vector<float> m_steps_size;        // les longueurs des marches sur le sol

};

#endif

