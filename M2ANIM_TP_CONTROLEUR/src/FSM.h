#ifndef FSM_H
#define FSM_H

#include <vector>

struct State {
    unsigned int ID;                    // Identificateur de l'�tat
    std::vector<float> targetAngles;    // Angle cl� pour chaque articulation
    std::vector<bool> targetLocal;      // Vrai si l'angle cl� est local au parent, faux sinon (global)
    unsigned int nextState;             // Identificateur de l'�tat suivant
    float transitionTime;               // La dur�e de transition (si bas� dur�e)
};

class FSM {

public:

	// Mise � jour de l'�tat si condition remplie
	void update(double Dt, std::vector<float> currentAnglesLocal, std::vector<float> currentAnglesGlobal, float definedTransitionTime);

    State interpole(int id_st0, int id_st1, double tmp);

	// Retourne les cibles et information local/global
	std::vector<float> getCurrentTargetAngles(unsigned int interpolation_type, float definedTransitionTime) const;
	std::vector<bool> getCurrentTargetLocal() const {return m_states[m_currentState].targetLocal;}

	// Retourne l'ID de l'�tat
	unsigned int getID() const {return m_states[m_currentState].ID;}

protected:

	FSM();      // Construit la machine avec ses �tats

public:
    unsigned int            m_nbStates;             // Le nombre d'�tats
    unsigned int            m_currentState;         // L'indice de l'�tat courant
    std::vector<State>      m_states;               // Les �tats
    double                  m_timeInState;          // Temps �coul� dans l'�tat
    std::vector<float>      m_anglesAtTransition;   // Les angles au moment de la derni�re transition

    // Pour interpolation :
    State                   pose_reelle;            // On veut sauvegarder les positions actuelles pour interpolation

};

class FSM_Stand : public FSM { // La machine � �tats finis pour la pose de base
public:
	FSM_Stand();
};

class FSM_Walk : public FSM { // La machine � �tats finis pour un mouvement de marche
public:
	FSM_Walk();
};

class FSM_Run : public FSM { // La machine � �tats finis pour un mouvement de course
public:
	FSM_Run();
};

#endif
